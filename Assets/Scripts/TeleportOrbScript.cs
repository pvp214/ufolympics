using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class TeleportOrbScript : MonoBehaviour
{
    public GameObject AirHockeyTeleport;
    public GameObject LockerRoomTeleport;
    public GameObject Alien;
    string lockerPath;
    string airHockeyPath;
    string tenisPath;
    string diskthrowingPath;

    bool airparentnull = true;
    bool lockerparentnull = true;

    Vector3 initialLockerTeleport;
    Vector3 initialAirhokeyTeleport;
    Vector3 initialtenisTeleport;

    // Start is called before the first frame update
    void Start()
    {
        lockerPath = SceneUtility.GetScenePathByBuildIndex(0);
        airHockeyPath = SceneUtility.GetScenePathByBuildIndex(1);
        diskthrowingPath = SceneUtility.GetScenePathByBuildIndex(2);

        initialLockerTeleport = LockerRoomTeleport.transform.position;
        initialAirhokeyTeleport = AirHockeyTeleport.transform.position;
    }

    private void Update()
    {

        if(!airparentnull && AirHockeyTeleport.gameObject.transform.parent == null)
        {
            AirHockeyTeleport.transform.position = initialAirhokeyTeleport;
            LockerRoomTeleport.transform.position = initialLockerTeleport;
            if (AirHockeyTeleport.gameObject.transform.parent == null)
            {
                airparentnull = true;
            }
           
        }
        if (!lockerparentnull && LockerRoomTeleport.gameObject.transform.parent == null)
        {
            AirHockeyTeleport.transform.position = initialAirhokeyTeleport;
            if(LockerRoomTeleport.gameObject.transform.parent == null)
            {
                lockerparentnull = true;
            }
            Destroy(LockerRoomTeleport);
        }
    }

    

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.root.name == "Player")
        {
            if (collision.gameObject.tag == "TeleportToHockey")
            {
                if (SceneManager.GetActiveScene().path != airHockeyPath)
                {
                    Destroy(gameObject.transform.root.gameObject);
                    StartCoroutine(LoadYourAsyncScene(airHockeyPath));
                }
                else
                {
                    Destroy(gameObject.transform.root.gameObject);
                    StartCoroutine(LoadYourAsyncScene(SceneManager.GetActiveScene().path));
                }
                airparentnull = false;
                Debug.Log("AirHockeyTeleport");
                transform.root.transform.position = new Vector3((float)-0.0450000018, (float)10.0080004, (float)-49.6730003);//(where you want to teleport)
                Alien.transform.position = new Vector3(-8.26000023f, 11.0100002f, -45.9399986f);
                airparentnull = false;
            }
            else if (collision.gameObject.tag == "TeleportToLockerRoom")
            {
                Debug.Log("LockerRoomTeleport");
                Alien.transform.position = new Vector3(3.05999994f, -7.053f, -2.20000005f);
                lockerparentnull = false;
                transform.root.transform.position = new Vector3(0, -7, 0);//(where you want to teleport)

            }
        }
    }

    IEnumerator LoadYourAsyncScene(string path)
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.
 
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(path);

            // Wait until the asynchronous scene fully loads
            while (!asyncLoad.isDone)
            {
                yield return null;
        }
        

    }

}