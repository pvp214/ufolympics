using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleWallController : MonoBehaviour
{
    private float speed;
    private float stepPerFrameOnZCordinates = 0;
    private Transform endPosition;
    private bool started;
    private void FixedUpdate()
    {
        if(started)
        {
            var newPosition = this.transform.position.z + stepPerFrameOnZCordinates;
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, newPosition);
            if(newPosition < endPosition.transform.position.z)
            {
                StopMoving(true);
                Debug.Log("Success");

            }
        }
    }

    public void StopMoving(bool shouldBeRemoved)
    {
        started = false;
        if(shouldBeRemoved)
        {
            WallsController.Instance.GenerateAndLaunchWall();
            GameObject.Destroy(this.gameObject);
        }
    }

    public void StepPerFrame()
    {
        var distanceToTravel = endPosition.transform.position.z - this.gameObject.transform.position.z;
        stepPerFrameOnZCordinates = distanceToTravel * speed;
        Debug.Log(stepPerFrameOnZCordinates);
    }

    public void StartMoving(Transform endPoint, float speed)
    {
        this.endPosition = endPoint;
        this.speed = speed;
        if (stepPerFrameOnZCordinates == 0)
            StepPerFrame();

        Debug.Log("Started moving");
        started = true;
    }    
}
