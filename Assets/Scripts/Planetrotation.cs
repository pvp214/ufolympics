using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planetrotation : MonoBehaviour
{
    public Celestial[] CelestialBodies;
    public float SunDegrees=0f;
    public float blueRelativeSunSpeed=2f;
    public float sandyRelativeSunSpeed = 0.2f;
    public float gasRelativeSunSpeed = 0.6f;
    public float habitableRelativeSunSpeed = 1.3f;
    Quaternion Originsun;
    Quaternion Originblue;
    Quaternion Originsandy;
    Quaternion Origingas;
    Quaternion Originhabitable;
    Quaternion Difference;



    private void Start()
    {
        CelestialBodies = GetComponentsInChildren<Celestial>();
        Originsun = CelestialBodies[0].transform.rotation;
        Originblue = CelestialBodies[1].transform.rotation;
        Originsandy = CelestialBodies[2].transform.rotation;
        Origingas = CelestialBodies[3].transform.rotation;
        Originhabitable = CelestialBodies[4].transform.rotation;
    }
    // Update is called once per frame
    void Update()
    {
        float sunangleinradians = SunDegrees * Mathf.PI / 180;
        float blueangleinradians = SunDegrees * blueRelativeSunSpeed * Mathf.PI / 180;
        float sandyangleinradians = SunDegrees * sandyRelativeSunSpeed * Mathf.PI / 180;
        float gasangleinradians = SunDegrees * gasRelativeSunSpeed * Mathf.PI / 180;
        float habitableangleinradians = SunDegrees * habitableRelativeSunSpeed * Mathf.PI / 180;

        CelestialBodies[0].transform.rotation = Originsun * new Quaternion(Mathf.Sin(sunangleinradians / 2), Mathf.Sin(0), Mathf.Sin(0), Mathf.Cos(sunangleinradians / 2));
        CelestialBodies[1].transform.rotation = Originblue * new Quaternion(Mathf.Sin(blueangleinradians / 2), Mathf.Sin(0), Mathf.Sin(0), Mathf.Cos(blueangleinradians / 2));
        CelestialBodies[2].transform.rotation = Originsandy * new Quaternion(Mathf.Sin(sandyangleinradians / 2), Mathf.Sin(0), Mathf.Sin(0), Mathf.Cos(sandyangleinradians / 2));
        CelestialBodies[3].transform.rotation = Origingas * new Quaternion(Mathf.Sin(gasangleinradians / 2), Mathf.Sin(0), Mathf.Sin(0), Mathf.Cos(gasangleinradians / 2));
        CelestialBodies[4].transform.rotation = Originhabitable * new Quaternion(Mathf.Sin(habitableangleinradians / 2), Mathf.Sin(0), Mathf.Sin(0), Mathf.Cos(habitableangleinradians / 2));
    }
}
