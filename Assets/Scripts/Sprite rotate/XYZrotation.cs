using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XYZrotation : MonoBehaviour
{
    Canvas canv;
    public GameObject target;
    private void Start()
    {
        canv = GetComponentInChildren<Canvas>();
    }
    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            Quaternion newQuat = Quaternion.LookRotation(target.transform.position - canv.transform.position, Vector3.up);
            canv.transform.rotation = Quaternion.Lerp(canv.transform.rotation, newQuat, Time.deltaTime);
        }
    }
}
