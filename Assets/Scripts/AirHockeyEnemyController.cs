using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AirHockeyEnemyController : MonoBehaviour
{
    public Transform hockeyBallTr;      // Kamuoliuko transform
    public Transform enemyHandleTr;     // Priešininko laikilio (didžiojo) transform
    public Rigidbody hockeyBallRb;      // Kamuoliuko rigidbody
    public Rigidbody enemyHandleRb;     // Priešininko laikiklio (didžiojo) rigidbody
    private float alertDistance = 35;   // Kokiu atstumu priešininkas pradeda taikytis į kamuoliuką
    private Vector3 hockeyBallPos;      // Esama kamuoliuko pozicija         
    private Vector3 enemyHandlePos;     // Esama priešininko laikiklio (didžiojo) pozicija
    private Vector3 dir;                // Kryptis į kurią turi judėti laikilis
    private float startZ;               // Pradinė z pozicija
    private float force = 70;           // Jėgos stiprumas. Nuo jo priklauso judėjimo greitis

    float timer=0;
    void Start()
    {
        // Pradžioje nustatome pradinę z poziciją, kad žinotume, 
        // kurioje linijoje priešininko laikiklis visada turi 
        // laukti kamuoliuko.
        startZ = enemyHandleTr.position.z;
    }

    void Update()
    {
        // Nustatome kamuoliuko bei laiklio esamas pozicijas.
        hockeyBallPos = hockeyBallTr.position;
        enemyHandlePos = enemyHandleTr.position;

        if ((enemyHandlePos - hockeyBallPos).magnitude < 5)
        {
            timer += Time.deltaTime;
        }
        else
        {
            timer = 0;
        }
 
        if (timer > 7){ enemyHandleTr.position = enemyHandlePos + new Vector3(0, 0, -10); }
        // Patiktiname ar kamuoliukas yra pakankamai arti IR 
        // jis nėra už savo arenos dalies ribų.
        if (Mathf.Abs(enemyHandleTr.position.z - hockeyBallTr.position.z) < alertDistance && enemyHandleTr.position.z > startZ - 18){
            // Jei taip, laikiklis gali judėti.
            dir = (hockeyBallPos - enemyHandlePos).normalized;
            enemyHandleRb.AddForce(dir * force);
            Vector3 dir2 = new Vector3(0,0,1);
            enemyHandleRb.AddForce(dir2 * force / 5);
        // Jei ne, laikiklis turi grįžti į pradinę z poziciją.
        } else {
            // Jei laikiklis yra prieš starto liniją, tai 
            // juda atgal.
            if (enemyHandleTr.position.z < startZ - 4){
                dir = new Vector3(0,0,1);
                enemyHandleRb.AddForce(dir * force);
            // Jei laikiklis yra už starto linijos, tai 
            // juda į priekį.
            } else if (enemyHandleTr.position.z > startZ + 4){
                dir = new Vector3(0,0,-1);
                enemyHandleRb.AddForce(dir * force);
            }
        }
    }
}
