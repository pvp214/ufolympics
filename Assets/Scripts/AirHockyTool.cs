using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirHockyTool : MonoBehaviour
{
    public Transform bigtool;
    public Transform smalltool;
    public Rigidbody bigtoolrb;
    public GameObject temp;

    // Update is called once per frame
    void Update()
    {
        Vector3 Move = new Vector3(smalltool.transform.localPosition.y * 0.5232f - bigtool.localPosition.y, 0,smalltool.transform.localPosition.x - bigtool.localPosition.x);
        //Debug.Log(Move*100);
        bigtoolrb.AddForce((-Move-bigtoolrb.velocity/500)*50,ForceMode.VelocityChange);
    }
}
