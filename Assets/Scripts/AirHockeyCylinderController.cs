using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirHockeyCylinderController : MonoBehaviour
{
    public Rigidbody hockeyBallRb;      // Kamuoliuko rigidbody
    public Transform hockeyBallTr;      // Kamuoliuko transform
    public Transform enemyHandleTr;     // Priešininko laikiklio (didžiojo) transform
    public Transform hockeyTableTr;     // The arena table transform
    public Transform playerHandleTr;    // The player's big handle transform
    public Rigidbody playerHandleRb;    // The player's big handle rigidbody
    public AirHockeyScoreController scoreController;    // The score controller
    private Vector3 hockeyBallPos;      // Kamuoliuko esama pozicija
    private Vector3 enemyHandlePos;     // Priešininko laikilio (didžiojo) esama pozicija
    private Vector3 dir;                 // Kryptis į kurią turi skristi kamuoliukas
    private float bounceForce = 30;     // Atšokimo jėga. Nuo to priklauso kamuoliuko greitis
    void OnCollisionEnter(Collision collision)
    {
        // Jei priešininko laikilis priliečia 
        // kamuoliuką, tada jis atšoka.
        if (collision.gameObject.name == "Airhockey_tool_enemy"){
            // Nustatome kryptį.
            hockeyBallPos = hockeyBallTr.position;
            enemyHandlePos = enemyHandleTr.position;
            dir = (hockeyBallPos - enemyHandlePos).normalized;
            // Pastumiame kamuoliuką į tą kryptį.
            hockeyBallRb.AddForce(dir * bounceForce);
        }
        // If the cylinder goes into the player goal then stop the cylinder,
        // reset it's position, move the enemy to it's starting position also.
        if (collision.gameObject.name == "AirHockeyPlayerGoal"){
            hockeyBallTr.position = new Vector3(hockeyTableTr.position.x - 11,
                hockeyTableTr.position.y + 1, 
                hockeyTableTr.position.z + 25);
            enemyHandleTr.position = new Vector3(hockeyTableTr.position.x + 11,
                hockeyTableTr.position.y + 1, 
                hockeyTableTr.position.z + 25);
            hockeyBallRb.velocity = new Vector3(0,0,0);
            // Increase the score for the enemy
            scoreController.increaseScore("enemy");
        }
        // If the cylinder goes into the enemy goal then stop the cylinder,
        // reset it's position, move the player to it's starting position also.
        if (collision.gameObject.name == "AirHockeyEnemyGoal"){
            hockeyBallTr.position = new Vector3(hockeyTableTr.position.x + 11,
                hockeyTableTr.position.y + 1, 
                hockeyTableTr.position.z - 25);
            hockeyBallRb.velocity = new Vector3(0,0,0);
            playerHandleTr.position = new Vector3(hockeyTableTr.position.x - 11,
                hockeyTableTr.position.y + 1, 
                hockeyTableTr.position.z - 25);
            hockeyBallRb.velocity = new Vector3(0,0,0);
            // Increase the score for the player
            scoreController.increaseScore("player");
        }
    }
}