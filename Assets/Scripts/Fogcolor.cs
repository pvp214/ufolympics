﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fogcolor : MonoBehaviour
{

    public Material Sky_eq;
    Color Skycol;
    Vector4 Skyint;
    float H, S, V;
    private IEnumerator Fogcheck;
    bool runcor = true;

    void Start()
    {
        Fogcheck = changefogcolor(1.0f);
        StartCoroutine(Fogcheck);
    }

    private IEnumerator changefogcolor(float waitTime)
    {
        while(runcor)
        {
            yield return new WaitForSeconds(waitTime);
            Skycol = Sky_eq.GetColor("Sky2");
            Skyint = Sky_eq.GetVector("SkyIntensity");
            Color.RGBToHSV(Skycol,out H,out S,out V);
            Color Fogcol = Color.HSVToRGB(H,S+0.06f,V*Skyint.x*2);
            RenderSettings.fogColor = Fogcol;
            
            //print(Skyint);
        }
    }
}
