using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HittingWallController : MonoBehaviour
{
    private bool started = false;
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            started = true;
            WallsController.Instance.GameState(true);
        } else if (Input.GetKeyDown(KeyCode.R))
        {
            started = true;
            WallsController.Instance.Restart();
            WallsController.Instance.GameState(true);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!started)
            return;
         if (other.gameObject.tag == "Wall")
        {
            other.gameObject.GetComponent<HoleWallController>().StopMoving(false);
            WallsController.Instance.GameState(false);
            Debug.Log("Game over");
        }
    }
}
