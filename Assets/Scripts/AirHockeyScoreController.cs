using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AirHockeyScoreController : MonoBehaviour
{
    public Textchatinairhockey tcah;
    public Transform playerGoalTr;      // Player goal cube transform
    public Transform enemyGoalTr;       // Enemy goal cube transform
    public Transform hockeyTableTr;     // Arena table transform
    public Transform playerScoreTextTr; // Player score text object transform
    public Transform enemyScoreTextTr;  // Enemy score text object transform
    public TextMeshProUGUI playerScoreText;    // Player score text object text mesh
    public TextMeshProUGUI enemyScoreText;     // Enemy score text object text mesh
    public Transform hockeyBallTr;      // Cylinder's transform
    public Transform enemyHandleTr;     // Enemy big handle transform
    public Rigidbody hockeyBallRb;      // Cylinder's rigidbody
    public Rigidbody enemyHandleRb;     // Enemy big handle rigidbody
    private int playerScore = 0;        // Player total score
    private int enemyScore = 0;         // Enemy total score
    private int scoreGoal = 2;          // The wining score

    bool resultonce = false;

    void Start()
    {
        // Displaying the current score.
        playerScoreText.text = playerScore.ToString();
        enemyScoreText.text = enemyScore.ToString();

        // Setting the initial positions of the goal cubes.
        playerGoalTr.position = new Vector3(hockeyTableTr.position.x, 
            hockeyTableTr.position.y + playerGoalTr.localScale.y, 
            hockeyTableTr.position.z - 41);
        enemyGoalTr.position = new Vector3(hockeyTableTr.position.x, 
            hockeyTableTr.position.y + enemyGoalTr.localScale.y, 
            hockeyTableTr.position.z + 41);
        playerScoreTextTr.position = new Vector3(hockeyTableTr.position.x + 33, 
            hockeyTableTr.position.y + 17, 
            hockeyTableTr.position.z);
        enemyScoreTextTr.position = new Vector3(hockeyTableTr.position.x - 33, 
            hockeyTableTr.position.y + 17, 
            hockeyTableTr.position.z);
    }
    void Update(){
        // If the player reached the winning score then display text.
        if (playerScore > scoreGoal){
            playerScoreText.text = "YOU WIN!";
            enemyScoreText.text = "Press r to restart.";
            if(!resultonce)
            {
                tcah.Won();
                resultonce = true;
            }

        }
        // If the enemy reached the winning score then display text.
        if (enemyScore > scoreGoal){
            playerScoreText.text = "Sorry, you lost...";
            enemyScoreText.text = "Press r to restart.";
            if (!resultonce)
            {
                tcah.Lost();
                resultonce = true;
            }
        }
        // If the winning score has been reached, then stop the cylinder 
        // from moving around.
        if (enemyScore > scoreGoal || playerScore > scoreGoal){
            // The r button is pressed then restart the score system.
            if (Input.GetKey(KeyCode.R)){
                playerScore = 0;
                enemyScore = 0;
                playerScoreText.text = playerScore.ToString();
                enemyScoreText.text = enemyScore.ToString();

                hockeyBallTr.position = new Vector3(hockeyTableTr.position.x + 11,
                hockeyTableTr.position.y + 1, 
                hockeyTableTr.position.z - 25);
                enemyHandleTr.position = new Vector3(hockeyTableTr.position.x + 11,
                hockeyTableTr.position.y + 1, 
                hockeyTableTr.position.z + 25);
                enemyHandleRb.velocity = new Vector3(0,0,0);

                resultonce = false;
            }
            hockeyBallRb.velocity = new Vector3(0,0,0);
        }
    }
    // Increases the score by 1 for the specified entity.
    public void increaseScore(string entity){
        if (entity.Equals("player")){
            playerScore += 1;
            playerScoreText.text = playerScore.ToString();
            tcah.RanodmQuipwinning();
        }
        if (entity.Equals("enemy")){
            enemyScore += 1;
            enemyScoreText.text = enemyScore.ToString();
            tcah.RanodmQuiploosing();
        }
    }
}
