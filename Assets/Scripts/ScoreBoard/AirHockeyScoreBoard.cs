using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AirHockeyScoreBoard : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private TextMeshPro playerPointCounter;
    [SerializeField]
    private TextMeshPro enemyPointCounter;
    [SerializeField]
    private TextMeshPro playerWinner;
    [SerializeField]
    private TextMeshPro enemyWinner;
    [SerializeField]
    private int TotalPointToVictory;
    //[SerializeField]
    //private Event winningEvent;
    private int playerCounter = 0;
    private int enemyCounter = 0;
    [HideInInspector]
    public AirHockeyScoreBoard Instance;
    private void Awake()
    {
        Instance = this;
        playerWinner.gameObject.SetActive(false);
        enemyWinner.gameObject.SetActive(false);
    }
    void Start()
    {
        playerPointCounter.text = "";
        enemyPointCounter.text = "";
    }

    public void NewPointScored(bool scoredByPlayer)
    {
        if (playerCounter == TotalPointToVictory || enemyCounter == TotalPointToVictory)
            ResetScore();
        if (scoredByPlayer)
            IncreasePlayerPoints();
        else
            IncreaseEnemyPoints();
        CheckIfAnyPlayerWon();
    }

    public void ResetScore()
    {
        playerCounter = 0;
        enemyCounter = 0;
        playerPointCounter.text = "";
        enemyPointCounter.text = "";
        playerWinner.gameObject.SetActive(false);
        enemyWinner.gameObject.SetActive(false);

    }

    private void CheckIfAnyPlayerWon()
    {
        if(playerCounter == TotalPointToVictory)
        {
            playerWinner.gameObject.SetActive(true);
        }
        else if(enemyCounter == TotalPointToVictory)
        {
            enemyWinner.gameObject.SetActive(true);
        }
    }

    private void IncreasePlayerPoints()
    {
        playerCounter++;
        playerPointCounter.text += "O ";
    }

    private void IncreaseEnemyPoints()
    {
        enemyCounter++;
        enemyPointCounter.text += "O ";
    }
}
