﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[ExecuteInEditMode]
public class DayNightCycle : MonoBehaviour
{
    private float decimalTime = 0.0f;
    public float DecimalTime { get { return decimalTime; } private set { decimalTime = value; } }

    Transform sunpos;
    public float cycleInMinutes = 60;
    private float sunAngle;
    private float sunAngle2;

    public UnityEvent onMidnight;
    public UnityEvent onMorning;
    public UnityEvent onNoon;
    public UnityEvent onEvening;

    private enum TimeOfDay { Night, Morning, Noon, Evening }

    private TimeOfDay timeOfDay = TimeOfDay.Night;
    private TimeOfDay TODMessageCheck = TimeOfDay.Night;


    public AnimationCurve sunBrightness = new AnimationCurve(
new Keyframe(0, 0.01f),
new Keyframe(0.15f, 0.01f),
new Keyframe(0.35f, 1),
new Keyframe(0.65f, 1),
new Keyframe(0.85f, 0.01f),
new Keyframe(1, 0.01f)
);

    public AnimationCurve skyIntensity = new AnimationCurve(
new Keyframe(0, 0.2f),
new Keyframe(0.10f, 0.2f),
new Keyframe(0.35f, 0.3f),
new Keyframe(0.65f, 0.3f),
new Keyframe(0.90f, 0.2f),
new Keyframe(1, 0.2f)
);

    [GradientUsage(true)]
    public Gradient skyColorDay = new Gradient()
    {
        colorKeys = new GradientColorKey[4]{
            new GradientColorKey(new Color(0.47f, 0.48f, 0.13f), 0),
            new GradientColorKey(new Color(0.21f, 0.36f, 0.74f), 0.3f),
            new GradientColorKey(new Color(0.21f, 0.36f, 0.74f), 0.7f),
            new GradientColorKey(new Color(0.47f, 0.48f, 0.13f), 1),
        },
        alphaKeys = new GradientAlphaKey[2]{
            new GradientAlphaKey(1, 0),
            new GradientAlphaKey(1, 1)
        }
    };

    [GradientUsage(true)]
    public Gradient skyColorNight = new Gradient()
    {
        colorKeys = new GradientColorKey[4]{
              new GradientColorKey(new Color(0.47f, 0.48f, 0.13f), 0),
            new GradientColorKey(new Color(0.21f, 0.36f, 0.74f), 0.3f),
            new GradientColorKey(new Color(0.21f, 0.36f, 0.74f), 0.7f),
            new GradientColorKey(new Color(0.47f, 0.48f, 0.13f), 1),
        },
        alphaKeys = new GradientAlphaKey[2]{
            new GradientAlphaKey(1, 0),
            new GradientAlphaKey(1, 1)
        }
    };

    public float starsSpeed = 8;

    private Light sunLight;
    private Planetrotation PR;

    // Start is called before the first frame update
    void Start()
    {
        sunLight = GetComponentInChildren<Light>();
        sunpos = sunLight.transform;
        PR = GetComponent<Planetrotation>();
    }

    void Update()
    {
        UpdateSunAngle();

        if (Application.isPlaying)
        {
            UpdatedecimalTime();
            sun_rotation();
            UpdateTimeOfDay();
        }

        SetSunBrightness();
        SetSkyColor();
        Setintensity();
        MoveStars();
    }

    void sun_rotation()
    {
        //sunpos.Rotate(Vector3.right * Time.deltaTime * 6 / cycleInMinutes);
        PR.SunDegrees += Time.deltaTime * 6 / cycleInMinutes;
    }

    void SetSunBrightness()
    {
        sunLight.intensity = sunBrightness.Evaluate(sunAngle);
    }

    float UpdateSunAngle()
    {
        sunAngle = Vector3.SignedAngle(Vector3.down, sunpos.forward, sunpos.right);
        sunAngle = sunAngle / 360 + 0.5f;
        return sunAngle;
    }

    void SetSkyColor()
    {
        if (sunAngle >= 0.25f && sunAngle < 0.75f)
            RenderSettings.skybox.SetColor("Sky2", skyColorDay.Evaluate(sunAngle * 2f - 0.5f));
//        else if (sunAngle > 0.75f)
//            (sunAngle >= 0.25f && sunAngle < 0.75f)
//{
//            RenderSettings.skybox.SetColor("Sky2", skyColorDay.Evaluate(sunAngle * 2f - 0.5f));
//        }
else if (sunAngle > 0.75f)
        {
            RenderSettings.skybox.SetColor("Sky2", skyColorNight.Evaluate(sunAngle * 2f - 1.5f));
        }
        else
        {
            RenderSettings.skybox.SetColor("Sky2", skyColorNight.Evaluate(sunAngle * 2f + 0.5f));
        }
    }

    void Setintensity()
    {
        Vector4 Intensity = new Vector4(skyIntensity.Evaluate(sunAngle), skyIntensity.Evaluate(sunAngle), skyIntensity.Evaluate(sunAngle), 1);
        RenderSettings.skybox.SetVector("SkyIntensity", Intensity);

    }

    void MoveStars()
    {
        sunAngle2 = Vector3.SignedAngle(Vector3.up, sunpos.forward, sunpos.right);
        sunAngle2 = sunAngle2 / 360 + 0.5f;
        RenderSettings.skybox.SetVector("StarsOffset", new Vector2((sunAngle2-0.25f) * starsSpeed, 0));
    }

    void UpdatedecimalTime()
    {
        decimalTime = (0.75f + Time.time * 6 / cycleInMinutes / 360) % 1;

    }

    void UpdateTimeOfDay()
    {

        if (decimalTime > 0.25 && decimalTime < 0.5f)
        {
            timeOfDay = TimeOfDay.Morning;
        }
        else if (decimalTime > 0.5f && decimalTime < 0.75f)
        {
            timeOfDay = TimeOfDay.Noon;
        }
        else if (decimalTime > 0.75f)
        {
            timeOfDay = TimeOfDay.Evening;
        }
        else
        {
            timeOfDay = TimeOfDay.Night;
        }

        if (TODMessageCheck != timeOfDay)
        {
            InvokeTimeOfDayEvent();
            TODMessageCheck = timeOfDay;
        }
    }

    void InvokeTimeOfDayEvent()
    {
        switch (timeOfDay)
        {
            case TimeOfDay.Night:
                if (onMidnight != null) onMidnight.Invoke();
                break;
            case TimeOfDay.Morning:
                if (onMorning != null) onMorning.Invoke();
                break;
            case TimeOfDay.Noon:
                if (onNoon != null) onNoon.Invoke();
                break;
            case TimeOfDay.Evening:
                if (onEvening != null) onEvening.Invoke();
                break;
        }
    }


}
