using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Textchat : MonoBehaviour
{
    public bool En = false;
    public bool End = false;

    bool firstlockerroom = true;
    bool firstsound = true;
    bool firstFOV = true;
    bool firstQuality = true;
    bool firstSearch = true;

    string Text;
    string shownText;
    int toChar;
    int fromChar=0;

    public AudioSource audioSource;
    public AudioClip saa;
    public AudioClip sao;
    public AudioClip sai;

    float speakingSpeed = 0.15f;
    float pauseSpeed = 2f;
    float CurrentTime;
    
    public TextMeshProUGUI TMP;
    // Start is called before the first frame update
    void Start()
    {
        Text = "Welcome to another galaxy. You are the chosen one. Your mission here is to represent the planet Earth in our sports game - Ufolympics. Let�s go into the vastness of this adventure.";
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position == new Vector3(3.05999994f, -7.053f, -2.20000005f))
        {
            if(firstlockerroom)
            {
                Text = "Welcome to the locker room.";
                fromChar = 0;
                toChar = 0;
                End = false;
                firstlockerroom = false;
            }
        }

        if (toChar != Text.Length && !End)
        {
            if (En)
            {
                shownText = "";
                CurrentTime += Time.deltaTime;

                if (Text[toChar] != '!' && Text[toChar] != '?' && Text[toChar] != ',' && Text[toChar] != '.')
                {

                    if (CurrentTime >= speakingSpeed)
                    {
                        toChar++;
                        CurrentTime = 0;

                        if (Text[toChar] != ' ')
                        {
                            float rngTo3 = Random.Range(0, 3);
                            if (rngTo3 < 1)
                            {
                                audioSource.clip = saa;
                            }
                            else if (rngTo3 < 2 && rngTo3 >= 1)
                            {
                                audioSource.clip = sai;
                            }
                            else
                            {
                                audioSource.clip = sao;
                            }
                            audioSource.Play();
                        }
                       
                    }

                    for (int i = fromChar; i < toChar; i++)
                    {
                        shownText += Text[i].ToString();
                    }

                    TMP.text = shownText;

                }
                else
                {
                    for (int i = fromChar; i <= toChar; i++)
                    {
                        shownText += Text[i].ToString();
                    }

                    TMP.text = shownText;

                    if (CurrentTime >= pauseSpeed)
                    {
                        toChar++;
                        CurrentTime = 0;
                        fromChar = toChar;
                    }

                }
            }
        }
        else
        {
            End = true;
            fromChar = 0;
            toChar = 0;
            TMP.text = "";
        }
        
    }

    public void Selectedsound()
    {
        Debug.Log("selected sound");
        if(firstsound)
        {
            Debug.Log("went to text");
            Text = "This changes the sound volume of the entire universe.";
            End = false;
            firstsound = false;
            fromChar = 0;
            toChar = 0;
        }

    }

    public void SelectedFOV()
    {
        if (firstFOV)
        {
            Text = "This changes field of vision of your eyes. Lower FOV zooms in your vision. Higher FOV zooms out your vision.";
            End = false;
            firstFOV = false;
            fromChar = 0;
            toChar = 0;
        }
    }

    public void SelectedQuality()
    {
        if (firstQuality)
        {
            Text = "This changes the quality at which your eyes see the world. This may improve brain performance.";
            End = false;
            firstQuality = false;
            fromChar = 0;
            toChar = 0;
        }
    }

    public void SelectedSearch()
    {
        if (firstSearch)
        {
            Text = "This searches for your audio and visual organs. this allows to change the parameters that suit you.";
            End = false;
            firstSearch = false;
            fromChar = 0;
            toChar = 0;
        }
    }
}
