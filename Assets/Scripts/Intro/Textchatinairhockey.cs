using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Textchatinairhockey : MonoBehaviour
{
    public bool En = false;
    public bool End = false;

    bool firstHockey = true;

    string Text;
    string shownText;
    int toChar;
    int fromChar=0;

    public AudioSource audioSource;
    public AudioClip saa;
    public AudioClip sao;
    public AudioClip sai;
    public Transform Player;

    float speakingSpeed = 0.15f;
    float pauseSpeed = 2f;
    float CurrentTime;
    
    public TextMeshProUGUI TMP;
    private void Start()
    {
        Text = "Let the game begin.";
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position ==  new Vector3(-8.26000023f, 11.0100002f, -45.9399986f))
        {
            En = true;
            if(firstHockey)
            {
                Text = "Let the game begin.";
                End = false;
                firstHockey = false;
                fromChar = 0;
                toChar = 0;
            }
        }

        if (toChar != Text.Length && !End)
        {
            if (En)
            {
                shownText = "";
                CurrentTime += Time.deltaTime;

                if (Text[toChar] != '!' && Text[toChar] != '?' && Text[toChar] != ',' && Text[toChar] != '.')
                {

                    if (CurrentTime >= speakingSpeed)
                    {
                        toChar++;
                        CurrentTime = 0;

                        if (Text[toChar] != ' ')
                        {
                            float rngTo3 = Random.Range(0, 3);
                            if (rngTo3 < 1)
                            {
                                audioSource.clip = saa;
                            }
                            else if (rngTo3 < 2 && rngTo3 >= 1)
                            {
                                audioSource.clip = sai;
                            }
                            else
                            {
                                audioSource.clip = sao;
                            }
                            audioSource.Play();
                        }
                       
                    }

                    for (int i = fromChar; i < toChar; i++)
                    {
                        shownText += Text[i].ToString();
                    }

                    TMP.text = shownText;

                }
                else
                {
                    for (int i = fromChar; i <= toChar; i++)
                    {
                        shownText += Text[i].ToString();
                    }

                    TMP.text = shownText;

                    if (CurrentTime >= pauseSpeed)
                    {
                        toChar++;
                        CurrentTime = 0;
                        fromChar = toChar;
                    }

                }
            }
        }
        else
        {
            End = true;
            fromChar = 0;
            toChar = 0;
            TMP.text = "";
        }
        
    }

    public void RanodmQuiploosing()
    {
            switch((int)Mathf.Round(Random.Range(-0.4f,2.49f)))
            {
                case 0:
                Text = "Try to make the movements as accurate as possible.";
                break;
            case 1:
                Text = "This will earn you more points.";
                break;
            case 2:
                Text = "Look, you have to raise your right hand higher.";
                break;
            default:
                Text = "Try to make the movements as accurate as possible.";
                break;
            }
            End = false;
            fromChar = 0;
            toChar = 0;
    }

    public void RanodmQuipwinning()
    {
        switch ((int)Mathf.Round(Random.Range(-0.4f, 3.49f)))
        {
            case 0:
                Text = "That is it! You got it!";
                break;
            case 1:
                Text = "This will surely earn us victory!";
                break;
            case 2:
                Text = "Nice form! Keep it up!";
                break;
            case 3:
                Text = "Accurate shot!";
                break;
            default:
                Text = "Accurate shot!";
                break;
        }
        End = false;
        fromChar = 0;
        toChar = 0;
    }

    public void Won()
    {
       
        Text = "Congrats! You won this match.";
        End = false;
        fromChar = 0;
        toChar = 0;
    }
    public void Lost()
    {
       
        Text = "Try again.";
        End = false;
        fromChar = 0;
        toChar = 0;
    }
}
