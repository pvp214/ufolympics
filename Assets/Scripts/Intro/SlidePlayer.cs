using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlidePlayer : MonoBehaviour
{
    public bool En = false;
    int currentslide;

    float pausetime = 10f;
    float CurrentTime;

    float currentEnableTime = 0;
    float timeUntilEnable = 10;

    public Image image;
    public List<Sprite> slides;


    // Update is called once per frame
    void Update()
    {
        if(!En)
        {
            currentEnableTime += Time.deltaTime;
            if (currentEnableTime >= timeUntilEnable)
            {
                En = true;
                currentEnableTime = 0;
            }
        }
        

        if (currentslide != slides.Count)
        {
            if (En)
            {
                image.transform.localScale = Vector3.MoveTowards(image.transform.localScale, new Vector3(1,1,1), Time.deltaTime * 5);

                CurrentTime += Time.deltaTime;

                image.sprite = slides[currentslide];

                if (CurrentTime >= pausetime)
                {
                    currentslide++;
                    CurrentTime = 0;
                }
            }
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
