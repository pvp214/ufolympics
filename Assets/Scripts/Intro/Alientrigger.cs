using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alientrigger : MonoBehaviour
{
    public bool alientrigger = false;
    public GameObject Alien;
    public GameObject Player;
    public Textchat tc;
    public GameObject teleport;
    [SerializeField]
    private List<Transform> path;
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private XZrotation alienrot;

    int point=0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(alientrigger)
        {
            if (point < path.Count)
            {
                Vector3 newPos;

                newPos = Vector3.MoveTowards(Alien.transform.position, path[point].position, Time.deltaTime*5);

                animator.SetFloat("Blend", 0.75f);
                Alien.transform.position = newPos;
                Quaternion newQuat = Quaternion.LookRotation(path[point].position - Alien.transform.position, Vector3.up);
                Alien.transform.rotation = Quaternion.Lerp(Alien.transform.rotation,newQuat, Time.deltaTime * 5);

                if ((Alien.transform.position - path[point].position).magnitude <= 0.1)
                {
                    point++;
                }
            }
            else
            {
                Vector3 playerPosHeightNull = new Vector3(Player.transform.position.x, 0, Player.transform.position.z);
                Vector3 alienPosHeightNull = new Vector3(Alien.transform.position.x, 0, Alien.transform.position.z);

                //if (!tc.End)
                //{
                //    if ((alienPosHeightNull - playerPosHeightNull).magnitude > 10)
                //    {
                //        Vector3 newPos;
                //        newPos = Vector3.MoveTowards(Alien.transform.position, Player.transform.position, Time.deltaTime * 5);

                //        Vector2 blendVec = new Vector2(animator.GetFloat("Blend"), 0);
                //        animator.SetFloat("Blend", Vector2.MoveTowards(blendVec, new Vector2(0.75f, 0), Time.deltaTime * 5)[0]);
                        

                //        Alien.transform.position = new Vector3(newPos.x, Alien.transform.position.y, newPos.z);
                //    }
                //    else if ((Alien.transform.position - Player.transform.position).magnitude < 5)
                //    {
                //        Vector3 newPos;
                //        newPos = Vector3.MoveTowards(alienPosHeightNull, 2 * alienPosHeightNull - playerPosHeightNull, Time.deltaTime * 5);
                //        Vector2 blendVec = new Vector2(animator.GetFloat("Blend"), 0);
                //        animator.SetFloat("Blend", Vector2.MoveTowards(blendVec, new Vector2(0.25f, 0), Time.deltaTime * 5)[0]);

                //        Alien.transform.position = new Vector3(newPos.x,Alien.transform.position.y,newPos.z);
                //    }
                //    else
                //    {
                        Vector2 blendVec = new Vector2(animator.GetFloat("Blend"), 0);
                        animator.SetFloat("Blend", Vector2.MoveTowards(blendVec, new Vector2(0.5f, 0), Time.deltaTime * 5)[0]);
                //    }
                //}


                Quaternion newQuat = Quaternion.LookRotation(playerPosHeightNull - alienPosHeightNull, Vector3.up);
                Alien.transform.rotation = Quaternion.Lerp(Alien.transform.rotation, newQuat, Time.deltaTime * 5);
                tc.En = true;
            }
        }
        if(tc.End)
        {
            teleport.transform.localScale = Vector3.MoveTowards(teleport.transform.localScale, new Vector3(1, 1, 1), Time.deltaTime * 5);
            if (teleport.transform.localScale == new Vector3(1, 1, 1))
            {
                alienrot.enabled = true;
                gameObject.SetActive(false);
            }
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.transform.root.name == "Player" && alientrigger == false)
        {
            alientrigger = true;
        }

    }
}
