using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallsController : MonoBehaviour
{
    public static WallsController Instance;
    [SerializeField]
    private GameObject[] walls;
    [SerializeField]
    private Transform startingPoint;
    [SerializeField]
    private Transform endPoint;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float speedIncrease;
    [SerializeField]
    private float maxSpeed;
    [SerializeField]
    private GameObject parent;

    private GameObject lastWall;
    private void Start()
    {
        Instance = this;
    }
    private bool gameRunning = false;
    public void GenerateAndLaunchWall()
    {
        if(!gameRunning)
            return;

        var selectedWall = walls[Random.Range(0, walls.Length)];
        lastWall = GameObject.Instantiate(selectedWall, parent.transform);
        lastWall.transform.position = startingPoint.position;
        lastWall.GetComponent<HoleWallController>().StartMoving(endPoint, speed);
        speed += speedIncrease;
        if(speed > maxSpeed)
            speed = maxSpeed;
    }

    public void GameState(bool gameState)
    {
        if(gameRunning && gameState)
            return;

        gameRunning = gameState;
        if(gameRunning)
        {
            GenerateAndLaunchWall();
        }
    }

    public void Restart()
    {
        GameObject.Destroy(lastWall);
        gameRunning = false;
    }
}
