using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class DiskThrowingScores : MonoBehaviour
{
    public TextMeshProUGUI score;
    public GameObject Disk;
    private Vector3 StartPos;

    private void Start()
    {
        StartPos = Disk.transform.position;
        score.text = "0";
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Disk")
        {
            score.SetText("Current: {0:3}", (Disk.transform.position - StartPos).magnitude);
        }
    }
}
