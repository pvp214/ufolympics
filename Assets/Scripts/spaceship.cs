using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spaceship : MonoBehaviour
{
    public GameObject door;
    public GameObject Player = null;
    [SerializeField]
    private List<Transform> path;
    [SerializeField]
    private List<GameObject> teleportareas;
    public List<ParticleSystem> PS;
    public AudioSource audio;
    public AudioSource doorsAudio;
    [SerializeField]
    private List<GameObject> Lines;
    [SerializeField]
    private List<Animator> Animators;
    int point = 0;
    float time;

    // Update is called once per frame
    void Update()
    {
        if(point < path.Count)
        {
            Vector3 newPos = new Vector3();
            if(point == 1)
            newPos = Vector3.Lerp(transform.position, path[point].position, Time.deltaTime/5); 
            else
            newPos = Vector3.Lerp(transform.position, path[point].position, Time.deltaTime/5);

            transform.position = newPos;

            if ((transform.position - path[point].position).magnitude <= 2 && point == 1)
            {
                point++;
            }

            if ((transform.position - path[point].position).magnitude <=0.1)
            {
                point++;
            }
        }
        else
        {
            time += Time.deltaTime;
            if(time == 5)
            {
                door.transform.rotation = Quaternion.Euler(10, 0, 0);
                point++;
            }

            door.transform.rotation = Quaternion.Lerp(door.transform.rotation, Quaternion.Euler(10,0,0), Time.deltaTime);


            if (Player.transform.parent != null)
            {
                Player.transform.parent = null;
                audio.enabled = false;
                doorsAudio.enabled = true;
            }

            if (Quaternion.Dot(door.transform.rotation, Quaternion.Euler(10, 0, 0))>0.99999)
            {
                doorsAudio.enabled = false;
            }

           

            if (Lines.Count > 0)
                if (!Lines[Lines.Count - 1].activeInHierarchy)
                {
                    foreach (GameObject line in Lines)
                    {
                        line.SetActive(true);
                    }
                }

            if (!teleportareas[teleportareas.Count - 1].activeInHierarchy)
            {
                foreach (GameObject teleportarea in teleportareas)
                {
                    teleportarea.SetActive(true);
                }
            }

            if (PS[PS.Count-1].isPlaying)
            {
                foreach (ParticleSystem particleSys in PS)
                {
                    particleSys.Stop();
                }
            }

            foreach(Animator animator in Animators)
            {
                animator.SetBool("End", true);
            }
        }
        

    }
}
