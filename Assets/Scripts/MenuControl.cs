using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

public class MenuControl : MonoBehaviour
{
    public GameObject Player;
    public AudioMixer mixer;
    Camera[] caminplayer;


    Camera cam;

    public Slider volumeSlider;
    //public Slider FOVSlider;
    public TMP_Dropdown Quality;

    // Start is called before the first frame update
    void Start()
    {
        caminplayer = Player.GetComponentsInChildren<Camera>();

        foreach (Camera cams in caminplayer)
        {
            if (cams.isActiveAndEnabled)
            {
                cam = cams;
            }
        }
        float intermediatevol;
        mixer.GetFloat("MusicVol", out intermediatevol);
        volumeSlider.value = Mathf.Pow(10, intermediatevol/20);
        //FOVSlider.value = cam.fieldOfView;
        Quality.value = QualitySettings.GetQualityLevel();
    }

    public void Exitapp()
    {
        Application.Quit();
    }

    public void volumeset(float volume)
    {
        mixer.SetFloat("MusicVol", Mathf.Log10(volume) *20);
    }

    //public void FOVset(float fov)
    //{
    //    cam.fieldOfView = fov;
    //}

    public void Search()
    {
        caminplayer = Player.GetComponentsInChildren<Camera>();

        foreach (Camera cams in caminplayer)
        {
            if (cams.isActiveAndEnabled)
            {
                cam = cams;
            }
        }
        //FOVSlider.value = cam.fieldOfView;
    }
    public void QualitySet(int quality)
    {
        QualitySettings.SetQualityLevel(quality);
    }

    public void ResetLevel()
    {
       

            
                StartCoroutine(LoadYourAsyncScene(SceneManager.GetActiveScene().path));

        

        
    }

    IEnumerator LoadYourAsyncScene(string path)
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(path);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            Destroy(Player);
            yield return null;
        }


    }
}
