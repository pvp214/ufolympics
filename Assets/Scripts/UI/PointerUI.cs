using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PointerUI : MonoBehaviour
{
    public float m_DefaultLength = 5.0f;
    public VRUIinputmodule m_InputModule;

    private LineRenderer m_lineRenderer = null;

    private void Awake()
    {
        m_lineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateLine();
    }

    private void UpdateLine()
    {
        PointerEventData data = m_InputModule.GetData();
        float targetLength = data.pointerCurrentRaycast.distance == 0 ? m_DefaultLength : data.pointerCurrentRaycast.distance;
        bool enabledline = (data.pointerCurrentRaycast.distance > 10 || data.pointerCurrentRaycast.distance == 0) ? false : true;

        RaycastHit hit = CreateRaycast(targetLength);

        Vector3 endpoint = transform.position + (transform.forward * targetLength);

        Debug.Log(data.pointerCurrentRaycast.distance);

        m_lineRenderer.enabled =enabledline;
        m_lineRenderer.SetPosition(0, transform.position);
        m_lineRenderer.SetPosition(1, endpoint);
    }

    private RaycastHit CreateRaycast(float length)
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);
        Physics.Raycast(ray, out hit, m_DefaultLength);

        return hit;
    }
}
